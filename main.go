package sniffer

import (
	"log"
	"fmt"
)

// this is my favorite Golang application, this will sniff the website
// what you like or interest in, and it return the information you just like
// I installed this on Sia, so that my chat bot would tell me what is happening
// outside the world automatically


func init() {
	log.SetPrefix("[main center]")
	log.SetFlags(log.Ldate | log.Lshortfile)
}


func main() {
	fmt.Println("Sniffer - sniff the websites you care about.")
}
